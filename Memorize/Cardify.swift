//
//  Cardify.swift
//  Memorize
//
//  Created by John R. Martin on 6/1/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SwiftUI

struct Cardify: AnimatableModifier {
    private var rotation: Double

    var animatableData: Double {
        get { rotation }
        set { rotation = newValue }
    }

    var isFaceup: Bool { rotation < 90 }

    init(isFaceup: Bool) {
        rotation = isFaceup ? 0 : 180
    }

    func body(content: Content) -> some View {
        ZStack {
            Group {
                RoundedRectangle(cornerRadius: cornerRadius).fill(Color.white)
                RoundedRectangle(cornerRadius: cornerRadius).stroke(lineWidth: edgeLineWidth)
                content
            }
            .opacity(isFaceup ? 1 : 0)
            RoundedRectangle(cornerRadius: cornerRadius).fill()
                .opacity(isFaceup ? 0 : 1)
        }
        .rotation3DEffect(.degrees(rotation), axis: (0,1,0))
    }
    // Mark: - Drawing Constant(s)
    private let cornerRadius: CGFloat = 10
    private let edgeLineWidth: CGFloat = 3
}

extension View {
    func cardify(isFaceup: Bool) -> some View {
        self.modifier(Cardify(isFaceup: isFaceup))
    }
}

