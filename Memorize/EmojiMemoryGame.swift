//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by John R. Martin on 5/26/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SwiftUI
typealias MemoryGameType = MemoryGame<String>
typealias MemoryGameCard = MemoryGameType.Card

class EmojiMemoryGame: ObservableObject {
    @Published private var model: MemoryGameType = EmojiMemoryGame.createMemoryGame()

    private static func createMemoryGame() -> MemoryGameType {
        let emojis = ["👻", "🎃", "🕷", "☠️", "👹", "😱"]
        return .init(numberOfPairsOfCards: emojis.count) { emojis[$0] }
    }

    var cards: Array<MemoryGameCard> { model.cards }

    func choose(card: MemoryGameCard) {
        model.choose(card: card)
    }

    func resetGame() {
        model = EmojiMemoryGame.createMemoryGame()
    }
}
