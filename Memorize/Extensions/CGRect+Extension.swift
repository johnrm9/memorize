//
//  CGRect+Extension.swift
//  Memorize
//
//  Created by John R. Martin on 6/7/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension CGRect {
    var center: CGPoint {
        .init(x: self.midX, y: self.midY)
    }

    var radius: CGFloat {
        min(self.height, self.width) / 2
    }
}
