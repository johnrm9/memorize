//
//  Array+Identifiable.swift
//  Memorize
//
//  Created by John R. Martin on 5/26/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

extension Array where Element: Identifiable {
    func firstIndex(matching: Element) -> Int? {
        for index in self.indices {
            if self[index].id == matching.id {
                return index
            }
        }
        return nil
    }
}
