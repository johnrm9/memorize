//
//  Array+Only.swift
//  Memorize
//
//  Created by John R. Martin on 5/26/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

extension Array {
    var only: Element? {
        count == 1 ? first : nil
    }
}
