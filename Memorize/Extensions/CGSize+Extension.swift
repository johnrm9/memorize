//
//  CGSize+Extension.swift
//  Memorize
//
//  Created by John R. Martin on 6/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import UIKit

extension CGSize {
    var fit: CGFloat { min(self.height, self.width) }
}
