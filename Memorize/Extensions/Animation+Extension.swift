//
//  Animation+Extension.swift
//  Memorize
//
//  Created by John R. Martin on 6/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SwiftUI

extension Animation {
    static var repeatsForever: Animation {
        repeatsForever()
    }
    static func repeatsForever(duration: Double = 1) -> Animation {
        Animation.linear(duration: duration).repeatForever(autoreverses: false)
    }
}

