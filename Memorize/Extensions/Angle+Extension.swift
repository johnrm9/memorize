//
//  Angle+Extension.swift
//  Memorize
//
//  Created by John R. Martin on 6/6/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SwiftUI

extension Angle {
    var cosine: CGFloat {
        cos(CGFloat(self.radians))
    }
    var sine: CGFloat {
        sin(CGFloat(self.radians))
    }
}
