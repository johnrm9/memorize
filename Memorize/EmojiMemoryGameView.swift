//
//  EmojiMemoryGameView.swift
//  Memorize
//
//  Created by John R. Martin on 5/26/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    @ObservedObject var viewModel: EmojiMemoryGame

    var body: some View {
        NavigationView {
            VStack {
                Grid(viewModel.cards) { card in
                    CardView(card: card)
                        .onTapGesture {
                            withAnimation(.linear(duration: self.durationChooseCard)) {
                                self.viewModel.choose(card: card)
                            }
                    }
                    .padding(self.cardPadding)
                }
                .padding()
                .foregroundColor(.orange)
            }.navigationBarTitle("Memorize Game")
                .navigationBarItems(trailing:
                    Button("New Game") {
                        withAnimation(.easeInOut) {
                            self.viewModel.resetGame()
                        }
                    }
            )
        }
    }

    // MARK: - Animation Constants
    private let durationChooseCard: Double = 0.75
    // Mark: - Drawing Constant(s)
    private let cardPadding: CGFloat = 5
}

struct CardView: View {
    var card: MemoryGameCard

    var body: some View {
        GeometryReader { self.body(for: $0.size) }
    }

    @State private var animatedBonusRemaining: Double = 0

    private func startBonusTimeAnimation() {
        animatedBonusRemaining = card.bonusRemaining
        withAnimation(.linear(duration: card.bonusTimeRemaining)) {
            animatedBonusRemaining = 0
        }
    }

    @ViewBuilder
    private func body(for size: CGSize) -> some View {
        if card.isFaceup || card.unMatched {
            ZStack {
                Group {
                    if card.isConsumingBonusTime {
                        Pie(startAngle: .zero, endAngle: -animatedBonusRemaining*360)
                            .onAppear { self.startBonusTimeAnimation() }

                    } else {
                        Pie(startAngle: .zero, endAngle: -card.bonusRemaining*360)
                    }
                }
                .padding(piePadding).opacity(pieOpacity)

                Text(card.content)
                    .font(.system(size: size.fit * cardSizeFactor))
                    .rotationEffect(.degrees(card.rotationDegrees))
                    .animation(card.isMatched ? .repeatsForever : .default)
            }
            .cardify(isFaceup: card.isFaceup)
            .transition(.scale)
        }
    }
    // Mark: - Drawing Constant(s)
    private let piePadding: CGFloat = 5
    private let pieOpacity: Double = 0.4
    private let cardSizeFactor: CGFloat = 0.70
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let game = EmojiMemoryGame()
        game.choose(card: game.cards[0])
        return EmojiMemoryGameView(viewModel: game)
    }
}

