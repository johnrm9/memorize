//
//  Pie.swift
//  Memorize
//
//  Created by John R. Martin on 6/1/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import SwiftUI

struct Pie: Shape {
    var startAngle: Angle
    var endAngle: Angle
    var clockwise: Bool = false

    var animatableData: AnimatablePair<Double, Double> {
        get { .init(startAngle.radians, endAngle.radians) }
        set { startAngle = .radians(newValue.first); endAngle = .radians(newValue.second) }
    }

    func path(in rect: CGRect) -> Path {
        Path() {
            let center: CGPoint = rect.center
            let radius: CGFloat = rect.radius
            let start: CGPoint = CGPoint(
                x: center.x + radius * startAngle.cosine,
                y: center.y + radius * startAngle.sine
            )
            $0.move(to: center)
            $0.addLine(to: start)
            $0.addArc(
                center: center,
                radius: radius,
                startAngle: startAngle,
                endAngle: endAngle,
                clockwise: clockwise)
            $0.addLine(to: center)
        }
    }
}

extension Pie {
    init(startAngle: Double, endAngle: Double, clockwise: Bool = false) {
        self.startAngle = .degrees(startAngle - 90)
        self.endAngle = .degrees(endAngle - 90)
        self.clockwise = !clockwise
    }
}

