//
//  MemoryGame.swift
//  Memorize
//
//  Created by John R. Martin on 5/26/20.
//  Copyright © 2020 John R. Martin. All rights reserved.
//

import Foundation

struct MemoryGame<CardContent> where CardContent: Equatable {
    private(set) var cards: Array<Card>

    private var indexOfTheOneAndOnlyFaceupCard: Int? {
        get { cards.indices.filter { cards[$0].isFaceup }.only }
        set { for index in cards.indices { cards[index].isFaceup = index == newValue } }
    }

    mutating func choose(card: Card) {
        func matchCards(at firstIndex: Int, at secondIndex: Int) {
            if cards[firstIndex] == cards[secondIndex] {
                cards[firstIndex].isMatched = true
                cards[secondIndex].isMatched = true
            }
        }
        if let chosenIndex = cards.firstIndex(matching: card), !cards[chosenIndex].isFaceup, !cards[chosenIndex].isMatched {
            if let potentialMatchIndex = indexOfTheOneAndOnlyFaceupCard {
                matchCards(at: chosenIndex, at: potentialMatchIndex)
                cards[chosenIndex].isFaceup = true
            } else {
                indexOfTheOneAndOnlyFaceupCard = chosenIndex
            }
        }
    }

    init(numberOfPairsOfCards: Int, cardContentFactory: (Int) -> CardContent) {
        cards = Array<Card>()

        for pairIndex in 0..<numberOfPairsOfCards {
            let content = cardContentFactory(pairIndex)
            cards += [Card(content: content), Card(content: content)]
        }
        cards.shuffle()
    }

    struct Card: Identifiable {
        let id = UUID()
        var isFaceup: Bool = false {
            didSet {
                if isFaceup { startUsingBonusTime() } else { stopUsingBonusTime() }
            }
        }

        var isMatched: Bool = false {
            didSet { stopUsingBonusTime() }
        }

        let content: CardContent

        var unMatched: Bool { isMatched == false }

        static func ==(lhs: Card, rhs: Card) -> Bool { lhs.content == rhs.content }

        var rotationDegrees: Double {
            isMatched ? 360 : 0
        }

        private(set) var bonusTimeLimit: TimeInterval = 6

        private var faceupTime: TimeInterval {
            if let lastFaceupDate = self.lastFaceupDate {
                return pastFaceupTime + Date().timeIntervalSince(lastFaceupDate)
            } else {
                return pastFaceupTime
            }
        }

        private(set) var lastFaceupDate: Date?

        private(set) var pastFaceupTime: TimeInterval = 0

        var bonusTimeRemaining: TimeInterval {
            max( 0, bonusTimeLimit - faceupTime)
        }

        var bonusRemaining: Double {
            ( bonusTimeLimit > 0 && bonusTimeRemaining > 0 ) ? bonusTimeRemaining/bonusTimeLimit : 0
        }

        var hasEarnedBonus: Bool {
            isMatched && bonusTimeRemaining > 0
        }

        var isConsumingBonusTime: Bool {
            isFaceup && unMatched && bonusTimeRemaining > 0
        }

        private mutating func startUsingBonusTime() {
            if isConsumingBonusTime, lastFaceupDate == nil {
                lastFaceupDate = Date()
            }
        }

        private mutating func stopUsingBonusTime() {
            pastFaceupTime = faceupTime
            self.lastFaceupDate = nil
        }
    }
}
